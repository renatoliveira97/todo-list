import './App.css';
import { useState } from 'react';

import Form from './components/Form';
import TodoList from './components/TodoList';

function App() {

  const [list, setList] = useState([]);

  const AddTodo = (newTodo) => {
    if (newTodo !== undefined && newTodo !== "") {
      setList([...list, newTodo]);
    }    
  }

  const HandleTodo = (item) => {
    let arr = list.filter(x => x !== item);
    setList(arr);
  }

  return (
    <div className="App">
      <header className="App-header">
        <Form AddTodo={AddTodo}/>
        <TodoList list={list} HandleTodo={HandleTodo}/>
      </header>
    </div>
  );
}

export default App;
