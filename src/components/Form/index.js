import { useState } from 'react';

import './style.css';

const Form = ({AddTodo}) => {

    const [userInput, setUserInput] = useState("");

    const HandleClick = event => {
        event.preventDefault();
        AddTodo(userInput);
        setUserInput("");
    }

    return (
        <form>
            <input
                type="text"
                value={userInput}
                onChange={(event) => setUserInput(event.target.value)}
            />
            <button onClick={HandleClick}>Enviar</button>
        </form>
    );
}

export default Form;