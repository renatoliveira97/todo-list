import './style.css';

const TodoList = ({list, HandleTodo}) => {
    return (
        <ul>
            {list.map((item, index) => (
                <li key={index}>
                    <span>{item}</span>
                    <button onClick={() => HandleTodo(item)}>Concluir tarefa</button>
                </li>
            ))}
        </ul>
    );
}

export default TodoList;